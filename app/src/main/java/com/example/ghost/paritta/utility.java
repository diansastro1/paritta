package com.example.ghost.paritta;

/**
 * Created by ghost on 12/12/16.
 */

public class utility {

    public String milliSecondTimer (long timer){

        String finalTimer = "";
        String secondTimer = "";

        //konversi total durasi
        int hours   = (int) (timer / (1000*60*60));
        int minutes = (int) (timer % (1000*60*60)) / (1000*60);
        int second  = (int) (timer % (1000*60*60)) % (1000*60) / (1000*60);

        if (hours > 0) {
            finalTimer = hours + ":";
        }

        if (second < 10 ){
            secondTimer = "0" + second;
        } else {
            secondTimer = "" + second;
        }

        finalTimer = finalTimer + minutes + ":" +secondTimer;
        return finalTimer;
    }

    public int getProgressPercentage (long currentDuration, long totaDuration){
        Double percentage    = (double) 0;
        long currentSeconds  = (int) (currentDuration / 1000);
        long totalSeconds    = (int) (totaDuration / 1000);
        percentage           = (((double) currentSeconds) / totalSeconds) * 100;
        return percentage.intValue();
    }

    public int progressToTimer (int progress, int totalDuration){
        int currentDuration = 0;
        totalDuration       = (int) (totalDuration / 1000);
        currentDuration     = (int) ((((double) progress) / 100) * totalDuration);
        return currentDuration * 1000;
    }
}
