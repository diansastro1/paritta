package com.example.ghost.paritta;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by ghost on 06/12/16.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return new Tab1();
        } else if (position == 1){
            return new Tab2();
        } else return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
