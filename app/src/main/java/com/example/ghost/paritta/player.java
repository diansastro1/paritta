package com.example.ghost.paritta;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

/**
 * Created by ghost on 12/12/16.
 */

public class player extends Activity {

    private ImageButton play, forward, backward;
    private SeekBar seekbar;
    private TextView songCurrentDuration, songTotalDuration;
    private MediaPlayer mp;
    private Handler mHandler = new Handler();
    private int forwardTime  = 5000; //milliseconds
    private int backwardTime = 5000;
    private double startTime = 0;
    private double finalTime = 0;
    public static int oneTimeOnly = 0;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_layout1);

        play     = (ImageButton) findViewById(R.id.btnPlay);
        forward  = (ImageButton) findViewById(R.id.btnForward);
        backward = (ImageButton) findViewById(R.id.btnBackward);

        seekbar             = (SeekBar) findViewById(R.id.seekbar);
        songCurrentDuration = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDuration   = (TextView) findViewById(R.id.songTotalDurationLabel);
        seekbar.setClickable(false);
        mp   = MediaPlayer.create(this, R.raw.sample);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mp.isPlaying()){
                    if (mp != null){
                        mp.pause();
                        play.setImageResource(R.drawable.btn_play);
                    }
                } else {
                    if (mp != null){
                        mp.start();
                        play.setImageResource(R.drawable.btn_pause);
                        //Toast.makeText(player.this, "Memutar Audio", Toast.LENGTH_SHORT).show();

                        finalTime  = mp.getDuration();
                        startTime  = mp.getCurrentPosition();

                        if (oneTimeOnly == 0) {
                            seekbar.setMax((int) finalTime);
                            oneTimeOnly= 1;
                        }

                        songTotalDuration.setText(String.format("%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                                TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                        finalTime)))
                        );

                        songCurrentDuration.setText(String.format("%d min, %d sec",
                                TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                                TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                                startTime)))
                        );

                        seekbar.setProgress((int) startTime);
                        mHandler.postDelayed(UpdateSongTime, 100);

                    }
                }
            }
        });

        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int temp = (int) startTime;

                if ((temp + forwardTime) <= finalTime){
                    startTime = startTime + forwardTime;
                    mp.seekTo((int) startTime);
                    //Toast.makeText(player.this, "Mempercepat 5 detik", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(player.this, "Tidak bisa mempercepat ", Toast.LENGTH_SHORT).show();
                }
            }
        });

        backward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int temp = (int) startTime;

                if ((temp - backwardTime) >= 0){
                    startTime = startTime - backwardTime;
                    mp.seekTo((int) startTime);
                    //Toast.makeText(player.this, "Memperlambat 5 detik", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(player.this, "TIdak bisa memperlambat", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTime = mp.getCurrentPosition();
            songCurrentDuration.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) startTime)))
            );
            seekbar.setProgress((int)startTime);
            mHandler.postDelayed(this, 100);
        }
    };
}
